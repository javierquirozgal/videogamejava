package com.videjuego.tanque;

import javax.swing.*;
import java.awt.*;

public class Game  {

    private static JFrame Mainwindow;
    public static void main(String[] args) {

        Mainwindow=new JFrame();
        Mainwindow.setTitle("mi juego");
        Mainwindow.setLocationRelativeTo(null);
        Mainwindow.setResizable(false);
        Mainwindow.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        Screen screen=new Screen(Mainwindow.getPreferredSize());
        Mainwindow.add(screen);
        //Mainwindow.setDefaultLookAndFeelDecorated(true);

        Timer timer=new Timer(30,screen);

        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {

                try {
                    // 0 => "javax.swing.plaf.metal.MetalLookAndFeel"
                    // 3 => the Windows Look and Feel
                    String name = UIManager.getInstalledLookAndFeels()[3].getClassName();
                    UIManager.setLookAndFeel(name);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                Mainwindow.pack();

                Mainwindow.setVisible(true);
                Mainwindow.setSize(640,480);
                Textures.init();
                timer.start();
            }
        });
    }
}
