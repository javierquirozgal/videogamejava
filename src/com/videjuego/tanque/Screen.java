package com.videjuego.tanque;

import com.videjuego.tanque.element.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Screen  extends JPanel
        implements
        KeyListener,
        ActionListener
{

    Tank tank1;
    Tank tank2;
    Helicopter helicopter1,helicopter2,helicopter3,helicopter4;
    Rectangle spaceTank1,spaceTank2;
    Rectangle spaceHelicopter;
    Rectangle spaceBullets;
    ArrayList<ILiveObject> objects=new ArrayList<>();
    ArrayList<ILiveObject> helicopteros=new ArrayList<>();
    int temporizador=0;
    SimpleDateFormat formato=new SimpleDateFormat("mm:ss");

    int puntajeTanque1=0;
    int puntajeTanque2=0;
    boolean pausa=true;

    public Screen(Dimension dimension)
    {

        this.setSize(dimension);
        this.setPreferredSize(dimension);
        this.setFocusable(true);
        this.requestFocus();
        this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        this.setBackground(new Color(244,254,0));
        this.addKeyListener(this);
        //this.addMouseListener(new OnMouse());
        //this.addMouseMotionListener(new OnMouse());


        //bullets space
        spaceBullets=new Rectangle(0,0,640 ,480);
        //tank space
        spaceTank1=new Rectangle(0,0,200,240);
        spaceTank2=new Rectangle(0,240,200,240);



        //tanque
        tank1 =new Tank(spaceTank1,Textures.tanque);
        tank1.updateXY(0,74);
        tank1.updateWidthHeight(90,52);

        tank2 =new Tank(spaceTank2,Textures.tanque);
        tank2.updateXY(0,spaceTank2.y+74);
        tank2.updateWidthHeight(90,52);

        //helicoptero

        spaceHelicopter=new Rectangle(320,0,320,480);

        helicopter1=new Helicopter(spaceHelicopter,Textures.helicopter);
        helicopter1.updateXY(spaceHelicopter.x+145,227);
        helicopter1.updateWidthHeight(50,26);

        helicopter2=new Helicopter(spaceHelicopter,Textures.helicopter);
        helicopter2.updateXY(spaceHelicopter.x+145,227);
        helicopter2.updateWidthHeight(50,26);

        helicopter3=new Helicopter(spaceHelicopter,Textures.helicopter);
        helicopter3.updateXY(spaceHelicopter.x+145,227);
        helicopter3.updateWidthHeight(50,26);

        helicopter4=new Helicopter(spaceHelicopter,Textures.helicopter);
        helicopter4.updateXY(spaceHelicopter.x+145,227);
        helicopter4.updateWidthHeight(50,26);

        objects.add(tank1);
        objects.add(tank2);
        objects.add(helicopter1);
        objects.add(helicopter2);
        objects.add(helicopter3);
        objects.add(helicopter4);

        helicopteros.add(helicopter1);
        helicopteros.add(helicopter2);
        helicopteros.add(helicopter3);
        helicopteros.add(helicopter4);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if(pausa){
            temporizador+=30;
            if(temporizador>=10*1000){
                pausa=false;
                String mensaje="";
                if(puntajeTanque1==puntajeTanque2){
                    mensaje="Empate";
                }else if(puntajeTanque1>puntajeTanque2){
                    mensaje="Gana el player 1";
                }else {
                    mensaje="Gana el player 2";
                }
                JOptionPane.showMessageDialog(null, "Juego Terminado!! "+mensaje+", gracias por jugar.");

                System.exit(0);


            }


            g.drawString("Tiempo "+formato.format(new Date(temporizador)),10,10);
            g.drawString("Player 1 : "+puntajeTanque1,10,30);
            g.drawString("Player 2 : "+puntajeTanque2,10,40);
            for(ILiveObject o:objects)
            {
                o.redraw(g);
            }
        }


    }



    @Override
    public void actionPerformed(ActionEvent e) {
        helicopter1.acelerar();
        helicopter2.acelerar();
        helicopter3.acelerar();
        helicopter4.acelerar();

        tank1.actualizarBalas(helicopteros, new IColisionado() {
            @Override
            public void colisionado() {
                puntajeTanque1++;
            }
        });
        tank2.actualizarBalas(helicopteros, new IColisionado() {
            @Override
            public void colisionado() {
                puntajeTanque2++;
            }
        });

        repaint();
    }

    //KEY LISTENER

    @Override
    public void keyTyped(KeyEvent e) {
        //System.out.println("keycode "+e.getKeyCode());
    }

    @Override
    public void keyPressed(KeyEvent e) {




    }

    @Override
    public void keyReleased(KeyEvent e) {
        //System.out.println("keycode "+e.getKeyCode());

        switch (e.getKeyCode())
        {
            case KeyEvent.VK_SPACE:

                tank2.newShoot(spaceBullets);
                //System.out.println("espacio "+e.getKeyCode());
                break;
            case KeyEvent.VK_W:
                tank2.move(Direction.UP, LiveObject.DEFAULT_VELOCITY);
                if(!tank2.stillInsideParent())
                {
                    tank2.move(Direction.DOWN, LiveObject.DEFAULT_VELOCITY);
                }
                break;
            case KeyEvent.VK_S:
                tank2.move(Direction.DOWN, LiveObject.DEFAULT_VELOCITY);
                if(!tank2.stillInsideParent())
                {
                    tank2.move(Direction.UP, LiveObject.DEFAULT_VELOCITY);
                }
                break;
            case KeyEvent.VK_A:
                tank2.move(Direction.LEFT, LiveObject.DEFAULT_VELOCITY);
                if(!tank2.stillInsideParent())
                {
                    tank2.move(Direction.RIGHT, LiveObject.DEFAULT_VELOCITY);
                }
                break;

            case KeyEvent.VK_D:
                tank2.move(Direction.RIGHT, LiveObject.DEFAULT_VELOCITY);
                if(!tank2.stillInsideParent())
                {
                    tank2.move(Direction.LEFT, LiveObject.DEFAULT_VELOCITY);
                }
                break;
            case KeyEvent.VK_CONTROL:
                tank1.newShoot(spaceBullets);

                break;



            case KeyEvent.VK_UP:
                tank1.move(Direction.UP, LiveObject.DEFAULT_VELOCITY);
                if(!tank1.stillInsideParent())
                {
                    tank1.move(Direction.DOWN, LiveObject.DEFAULT_VELOCITY);
                }
                break;
            case KeyEvent.VK_DOWN:
                tank1.move(Direction.DOWN, LiveObject.DEFAULT_VELOCITY);
                if(!tank1.stillInsideParent())
                {
                    tank1.move(Direction.UP, LiveObject.DEFAULT_VELOCITY);
                }
                break;
            case KeyEvent.VK_RIGHT:
                tank1.move(Direction.RIGHT, LiveObject.DEFAULT_VELOCITY);
                if(!tank1.stillInsideParent())
                {
                    tank1.move(Direction.LEFT, LiveObject.DEFAULT_VELOCITY);
                }
                break;
            case KeyEvent.VK_LEFT:
                tank1.move(Direction.LEFT, LiveObject.DEFAULT_VELOCITY);
                if(!tank1.stillInsideParent())
                {
                    tank1.move(Direction.RIGHT, LiveObject.DEFAULT_VELOCITY);
                }
                break;

        }
        repaint();

    }


}

