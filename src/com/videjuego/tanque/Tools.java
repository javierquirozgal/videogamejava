package com.videjuego.tanque;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Tools  {

    private static Map<Integer,Integer> numeros=new HashMap<>();
    private static Random r= new Random();

    public static int getRandom()
    {
        int low = 1;
        int high = 5;

        int  res;
        do {

            if(numeros.size()==4)
            {
                numeros.clear();
            }
            res=r.nextInt(high-low) + low;

        }while (numeros.containsKey(res));
        numeros.put(res,res);

        return res;
    }
}

