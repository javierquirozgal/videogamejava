package com.videjuego.tanque;

import javax.swing.*;
import java.awt.*;

public class Textures {

    public static Image tanque;
    public static Image helicopter;
    public static Image bullet;
    public static void init(){
        //tanque=new ImageIcon(Textures.class.getResource("/Imagenes/Tanque.png" )).getImage();
    }
    static{
        tanque=new ImageIcon(Textures.class.getResource("/Imagenes/Tanque.png" )).getImage();
        helicopter=new ImageIcon(Textures.class.getResource("/Imagenes/helicoptero.png" )).getImage();
        bullet=new ImageIcon(Textures.class.getResource("/Imagenes/bala.png" )).getImage();
    }
}
