/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.videjuego.tanque.old;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import javax.swing.ImageIcon;
/**
 *
 * @author Estudiante
 */
public class Helicoptero {
    private Image Helicop;
    // Guarda la scordenas
    private int X;
    private int Y;

    private int velocidad_X;
    private int velocidad_Y;
    
    private int velo;

    private int limite_izquierda = 0;
    private int limite_derecha;
    private int limite_superior = 0;
    private int limite_inferior;

    public Helicoptero(int x, int y, int velocidad, String imagen) {
        //Inicializando las cordenadas
        this.X = x;
        this.Y = y;
        velo=velocidad;
        //Jalando la imagen de la animacion, deve tener formato png y 80 x 80 pixeles
        Helicop = new ImageIcon(getClass().getResource("/Imagenes/" + imagen)).getImage();
        
    }
    
     //Manejamos las dimensiones del panel
    public void LimitesXY(int width, int height) {
        limite_derecha = width - Helicop.getWidth(null);
        limite_inferior = height - Helicop.getHeight(null);
    }

    //Manejamos las coordenadas para poder lograr el desplazamiento 
    public void move() {
        X += velocidad_X;
        Y += velocidad_Y;

        // Cntrolamos el manejo de los limites del panel 
        if (X < this.limite_izquierda) {
            X = 0;
            velocidad_X = -velocidad_X;
        } else if (X > limite_derecha) {
            X = limite_derecha;
            velocidad_X = -velocidad_X;
        }

        if (Y < this.limite_superior) {
            Y = 0;
            velocidad_Y = -velocidad_Y;
        } else if (Y > limite_inferior) {
            Y = limite_inferior;
            velocidad_Y = -velocidad_Y;
        }
    }

    public void setVelocidadXY() {
        velocidad_X = 0;
        velocidad_Y = velo;
    }
    
    public void setVelocidadXY2() {
        velocidad_X = 2;
        velocidad_Y = velo;
    }

    public void dibujar(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.drawImage(Helicop, X, Y, null);
        //g2.drawImage(bote, X, Y, 50, 50, null);
        
    }
    
    public boolean Colision(int xbala, int ybala, int basebala, int alturabala, boolean estadobala){
        
        int x1=xbala;
        int y1=ybala;
        int b1=basebala;
        int h1=alturabala;
        
        
        int p1=X;
        int q1=Y;
        int b2=Helicop.getWidth(null);
        int h2=Helicop.getHeight(null);
        
        if (!estadobala) {
                return false;
        }
        
        //hallamos el punto (x2, y2) del rectángulo 1 basados en su base y altura
        int x2, y2;
        x2 = x1 + b1;
        y2 = y1 + h1;
//Deduce el punto (P2, Q2) del rectángulo 2
        int p2, q2;
        p2 = p1 + b2;
        q2 = q1 + h2;
//Chequea si hay colisión
        
        //01
        if (x1 >= p1 && x1 <= p2 && y1 >= q1 && y1 <= q2) {
            return true;
        }
        //02
        if (x2 >= p1 && x2 <= p2 && y1 >= q1 && y1 <= q2) {
            return true;
        }
        //03       
        if (x1 >= p1 && x1 <= p2 && y2 >= q1 && y2 <= q2) {
            return true;
        }
        //04
        if (x2 >= p1 && x2 <= p2 && y2 >= q1 && y2 <= q2) {
            return true;
        }
        //05
        if (p1 >= x1 && p1 <= x2 && q1 >= y1 && q1 <= y2) {
            return true;
        }
        //06
        if (p2 >= x1 && p2 <= x2 && q1 >= y1 && q1 <= y2) {
            return true;
        }
        //07
        if (p1 >= x1 && p1 <= x2 && q2 >= y1 && q2 <= y2) {
            return true;
        }
        //08
        if (p2 >= x1 && p2 <= x2 && q2 >= y1 && q2 <= y2) {
            return true;
        }
//No se detectó colisión
        return false;
    }

    //Devuelve un numero aleatorio entre 1 y MAx
    /*private int getNumberRandom(int Max) {
        return (int) (Math.random() * Max + 1);
    }*/
}
