/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.videjuego.tanque.old;



import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
/**
 *
 * @author JeswsN
 */
public class Bala {
  private Image BalaImg;
    // Guarda la scordenas
    private int X;
    private int Y;
    public boolean Estado;

    private int velocidad_X;
    private int velocidad_Y;
    
    private int velo;

    private int limite_izquierda = 0;
    private int limite_derecha;
    private int limite_superior = 0;
    private int limite_inferior;

    public Bala(int x, int y, int velocidad) {
        //Inicializando las cordenadas
        this.X = x;
        this.Y = y;
        velo=velocidad;
        //Jalando la imagen de la animacion, deve tener formato png y 80 x 80 pixeles
        BalaImg = new ImageIcon(getClass().getResource("/Imagenes/bala.png" )).getImage();
        
    }
    
     //Manejamos las dimensiones del panel
    public void LimitesXY(int width, int height) {
        limite_derecha = width - BalaImg.getWidth(null);
        limite_inferior = height - BalaImg.getHeight(null);
    }

    //Manejamos las coordenadas para poder lograr el desplazamiento 
    public void move() {
        X += velocidad_X;
        Y += velocidad_Y;
        
        

        // Cntrolamos el manejo de los limites del panel 
        if (X > this.limite_derecha) {
            X = 0;
            Estado=false;
        } 
        

        
    }
    
    public void Dispara(int Xt, int Yt){
        Estado=true;
        //JOptionPane.showMessageDialog(null, Estado);
        X=Xt+50;
        Y=Yt;
        AudioClip sonido;
        sonido= Applet.newAudioClip(getClass().getResource("/Sonido/Efecto.wav"));
        sonido.play();
        
                //Applet.newAudioClip(
        
    }

    public void setVelocidadXY() {
        velocidad_X = velo;
        velocidad_Y = 0;
    }

    public void dibujar(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        //JOptionPane.showMessageDialog(null, Estado);
        if (Estado){
            g2.drawImage(BalaImg, X, Y, null);
        }
        //g2.drawImage(bote, X, Y, 50, 50, null);       
    }
    public int ObtenerX1(){
        return X;
    }
    
    public int ObtenerY1(){
        return Y;
    }
    
    public int ObtenerBase(){
        return BalaImg.getWidth(null);
    }
    
    public int ObtenerAltura(){
        return BalaImg.getHeight(null);
    }
    
    public boolean ObtenerEstado(){
        return Estado;
    }
    
    public void SetEstado(boolean Esta){
        Estado=Esta;
    }
}
