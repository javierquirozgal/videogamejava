/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.videjuego.tanque.old;


import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
/**
 *
 * @author JeswsN
 */
public class Tanque {
     private Image TanqueImg;
    // Guarda la scordenas
    private int X;
    private int Y;

    private int velocidad_X;
    private int velocidad_Y;
    
    private int Htanque;
    
    
    

    private int limite_izquierda = 0;
    private int limite_derecha;
    private int limite_superior = 0;
    private int limite_inferior;

    public Tanque(int x, int y, String imagen) {
        //Inicializando las cordenadas
        this.X = x;
        this.Y = y;
        
        velocidad_X = 5;
        velocidad_Y = 5;
        
        //Jalando la imagen de la animacion, deve tener formato png y 80 x 80 pixeles
        TanqueImg = new ImageIcon(getClass().getResource("/Imagenes/" + imagen)).getImage();
        Htanque=TanqueImg.getHeight(null);
        
        
    }
    
     //Manejamos las dimensiones del panel
    public void LimitesXY(int width, int height) {
        limite_derecha = 200 - TanqueImg.getWidth(null);
        limite_inferior = height - TanqueImg.getHeight(null);
    }

    //Manejamos las coordenadas para poder lograr el desplazamiento 
   

 

    public void dibujar(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.drawImage(TanqueImg, X, Y, null);
        //g2.drawImage(bote, X, Y, 50, 50, null);
        
    }
    
    public void TanqueArriba(){
        //JOptionPane.showMessageDialog(null, velocidad_Y);
        Y = Y - velocidad_Y;
        if (Y < 0) {
            Y = 0;
        }        
    }
    
    public void TanqueAbajo(){
        Y = Y + velocidad_Y;
        if (Y >= limite_inferior ) {
            Y = limite_inferior ;
        }  
    }
    
    public void TanqueIzquierda(){
        X = X - velocidad_X;
        if (X < 0) {
            X = 0;
        } 
    }
 
    public void TanqueDerecha(){
        X = X + velocidad_X;
        if (X >= limite_derecha ) {
            X = limite_derecha ;
        }  
    }
    
    public int XTanque(){
         
        return X;        
    }
    
    public int YTanque(){
         
        return Y;        
    }
    


}
