package com.videjuego.tanque.element;

import java.awt.*;

public interface ILiveObject {


    boolean stillInsideParent();
    Rectangle getSpace();
    void redraw(Graphics g);
}
