package com.videjuego.tanque.element;

import com.videjuego.tanque.Tools;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Helicopter extends LiveObject {

    private Direction dirVertical=Direction.UP;
    private Direction dirHorizontal=Direction.LEFT;
    private int DEFAULT_VELOCITY=3;
    public Helicopter(Rectangle parent, Image image) {
        super(parent, image);

        int r= Tools.getRandom();
        System.out.println("random "+r);
        if(r==1) {
            DEFAULT_VELOCITY=new Random().nextInt(3)+1;
            dirVertical=Direction.UP;
            dirHorizontal=Direction.RIGHT;
        }else if(r==2) {
            DEFAULT_VELOCITY=new Random().nextInt(3)+1;
            dirVertical=Direction.DOWN;
            dirHorizontal=Direction.LEFT;
        }else if(r==3){
            DEFAULT_VELOCITY=new Random().nextInt(3)+1;
            dirVertical=Direction.DOWN;
            dirHorizontal=Direction.RIGHT;
        }else if(r==4) {
            DEFAULT_VELOCITY=new Random().nextInt(3)+1;
            dirVertical=Direction.UP;
            dirHorizontal=Direction.LEFT;
        }
    }





    public void acelerar()
    {

        if(dirVertical==Direction.UP)
        {
            move(dirVertical,DEFAULT_VELOCITY);
            if(!stillInsideParent())
            {
                dirVertical=Direction.DOWN;
                move(dirVertical,DEFAULT_VELOCITY);
            }
        }
        else if(dirVertical==Direction.DOWN){
            move(dirVertical,DEFAULT_VELOCITY);
            if(!stillInsideParent())
            {
                dirVertical=Direction.UP;
                move(dirVertical,DEFAULT_VELOCITY);
            }
        }

        if(dirHorizontal==Direction.LEFT)
        {
            move(dirHorizontal,DEFAULT_VELOCITY);
            if(!stillInsideParent())
            {
                dirHorizontal=Direction.RIGHT;
                move(dirHorizontal,DEFAULT_VELOCITY);
            }
        }
        else if(dirHorizontal==Direction.RIGHT){
            move(dirHorizontal,DEFAULT_VELOCITY);
            if(!stillInsideParent())
            {
                dirHorizontal=Direction.LEFT;
                move(dirHorizontal,DEFAULT_VELOCITY);
            }
        }


    }
}
