package com.videjuego.tanque.element;

import javax.swing.*;
import java.awt.*;
import java.awt.image.ImageObserver;

public class LiveObject implements  ILiveObject{

    public static final int DEFAULT_VELOCITY=4;
    Rectangle space;
    int height;
    int width;
    int x;
    int y;
    Image image;
    Rectangle parent;
    public LiveObject(Rectangle parent, Image image)
    {
        space=new Rectangle();
        this.parent=parent;
        this.image=image;
        //this.image=new ImageIcon(getClass().getResource("/Imagenes/Tanque.png" )).getImage();
        if(this.image!=null)
        {
            System.out.println("ok");
        }
        else{
            System.out.println("bad");
        }
    }

    public void updateWidthHeight(int width,int height)
    {
        this.width=width;
        this.height=height;
        space.setSize(this.width,this.height);


    }
    public void updateXY(int x,int y)
    {
        this.x=x;
        this.y=y;
        space.setLocation(this.x,this.y);
    }

    public void move(Direction direction,int velocity)
    {
        if(Direction.UP==direction)
        {
            this.y+=((-1)*velocity);
        }else if(Direction.DOWN==direction){
            this.y+=velocity;
        }else if(Direction.RIGHT==direction){
            this.x+=velocity;
        }else if(Direction.LEFT==direction){
            this.x+=((-1)*velocity);
        }
        space.setLocation(this.x,this.y);
        //System.out.println("helicopter"+this.x+" - "+this.y);
    }

    @Override
    public void redraw(Graphics g){
        Graphics2D g2 = (Graphics2D) g;

        g2.drawImage(this.image, this.x, this.y, null);
    }
    @Override
    public boolean stillInsideParent() {

        return parent.contains(this.space);
    }

    public boolean isColision(Rectangle other) {

        return this.space.intersects(other);
    }

    @Override
    public Rectangle getSpace() {
        return this.space;
    }
}
