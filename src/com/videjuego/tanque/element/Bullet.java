package com.videjuego.tanque.element;

import java.awt.*;

public class Bullet extends LiveObject{
    private int DEFAULT_SPEED=7;
    private boolean alive=true;
    public Bullet(Rectangle parent, Image image){
        super(parent,image);
    }

    public void acelerar()
    {
        move(Direction.RIGHT,DEFAULT_SPEED);
        if(!stillInsideParent())
        {
            alive=false;
        }
    }

    public void collisioned() {
        alive=false;
        System.out.println("colisionado");
    }
    public boolean isAlive(){
        return alive;
    }

    @Override
    public void redraw(Graphics g) {
        super.redraw(g);

        //System.out.println(">"+this.x+" - "+ this.y);
    }
}