package com.videjuego.tanque.element;

import com.videjuego.tanque.Textures;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Tank extends LiveObject {

    private List<Bullet> bullets;
    public Tank(Rectangle parent, Image image) {
        super(parent, image);
        bullets=new ArrayList<>();
    }

    public void newShoot(Rectangle parent)
    {
        //todo arreglar alto ancho de imagen

        if(bullets.size()<=2)
        {
            Bullet bullet=new Bullet(parent, Textures.bullet);
            bullet.updateXY(this.x+this.width,(this.y+this.height/2));
            bullet.updateWidthHeight(20,20);
            bullets.add(bullet);
        }

    }


    public  void actualizarBalas(List<ILiveObject> helicopteros,IColisionado listener)
    {

        for(int k=bullets.size()-1;k>-1;k--)
        {

            if(bullets.get(k)!=null)
            {
                bullets.get(k).acelerar();

                for(ILiveObject helicoptero:helicopteros){
                    if(bullets.get(k).isColision(helicoptero.getSpace())) {
                        bullets.get(k).collisioned();
                        listener.colisionado();
                    }
                }

                if(!bullets.get(k).isAlive())
                bullets.remove(k);
            }
        }

    }
    @Override
    public void redraw(Graphics g) {
        super.redraw(g);

        for(Bullet b:bullets) {
            if(b!=null)
            b.redraw(g);
        }
    }
}
