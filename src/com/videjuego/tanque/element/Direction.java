package com.videjuego.tanque.element;

public enum  Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT
}
